public abstract class Cuenta
{
        private String nombre;
        private String apellido;
        private int cuenta;
        private String tipo;
        private String direccion;
        private int numero;
        
        // Get
        public String getNombre()
        {
                return nombre;
        }
        public String getApellido()
        {
                return apellido;
        }
        public int getCuenta()
        {
                return cuenta;
        }
        public String getTipo()
        {
                return tipo;
        }      
        public String getDireccion()
        {
                return direccion;
        }
        public int getNumero()
        {
                return numero;
        }    
        
        
        //Set
        public void setNombre(String nueva)
        {
                nombre = nueva;
        }
       
        public void setApellido(String nueva)
        {
                apellido = nueva;
        }
        
        public void setCuenta(int nueva)
        {
                cuenta = nueva;
        }
        
        public void setTipo(String nueva)
        {
                tipo = nueva;
        }
        
        public void setDireccion(String nueva)
        {
                direccion = nueva;
        }
        
        public void setNumero(int nueva)
        {
                numero = nueva;
        }
        
        
        public void NombreCompleto()
        {
                System.out.println("Nombre del Empleado");
                System.out.println(getNombre() + getApellido());
                
        }


}